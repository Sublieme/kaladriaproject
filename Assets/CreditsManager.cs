﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour
{
    public GameObject credits;
        void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {   if (credits.transform.position.y < 1500)
            credits.transform.position = new Vector2(credits.transform.position.x, credits.transform.position.y+1);
    }

    public void OnMouseDown() {
        if (credits.transform.position.y >= 1500)
            SceneManager.LoadScene("InitialScreen");
    }
}

Tiro Poderoso|Un gran disparo contra un enemigo adyacente|1|2.2|PAttack
Agilidad Animal|Aumenta la velocidad de una unidad adyacente.|1|0.3|Buff
Energía Vital|Recupera una pequeña cantidad de vida a una unidad cercana.|1|0.1|Heal
Ráfaga|Calem realiza un tiro a larga distancia de poca potencia.|6|0.2|PAttack
Halcón|Aumenta la velocidad en gran medida.|1|2.2|Buff
Gran Tiro|Un poderoso disparo que llega a todo el mapa.|10|0.8|PAttack
﻿Cuchilla mortal|Realiza un rápido ataque contra una unidad enemiga.|1|1|PAttack
Dolor Sagrado|La unidad seleccionada ve su resistencia reducida.|2|0.3|Debuff
Navajazo Astral|Un gran corte con el poder mágico de la daga.|1|1.2|MAttack
Gran Cura|Gran curación de una unidad cercana.|1|1|Heal
Corte Finall|Cuchillada mortal que puede acabar con casi cualquier oponente.|1|3.5|PAttack
Asesinar|La unidad seleccionada ve su ataque reducido enormemente.|2|1|Debuff
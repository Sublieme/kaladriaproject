﻿Background|Mountain
Calem:
La verdad es que no lucháis nada mal para ser...
Alke:
Apuestos?
Pleyanie:
Maravillosos, querrás decir.
Calem:
Iba a decir "jóvenes", pero veo que no tenéis abuela
Alke:
Perdona, pero la abuelita vive muy feliz en su casa.
Calem:
Creo que no era eso lo que...
Pleyanie:
¡Eh! ¡Allí están atacando a un clérigo!
Zac:
¡Morid, infieles!
Calem:
¿Estamos seguros de que necesita ayuda?
Alke:
A mí me gusta su estilo, y los otros son claramente
parte del ejército de la general Arla.
¡Así que yo me voy a apuntar a esa fiesta!
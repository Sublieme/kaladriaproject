﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMenu : MonoBehaviour {
    public void BSkill(int numberOfSkill) {
        BoardManager.instance.BSkillClicked(numberOfSkill);
    }

    public void BMove() {
        BoardManager.instance.BMoveClicked();
    }

    public void BEndTurn() { 

        BoardManager.instance.BEndTurnClicked();
    }
    public void BMenu() {
        BoardManager.instance.BMenuClicked();
    }
    public void Awake() {
        if (Screen.currentResolution.height >= 1440) {
            gameObject.transform.parent.transform.position = new Vector3(gameObject.transform.parent.position.x, gameObject.transform.parent.position.y - 60,10f);
        }
    }
}

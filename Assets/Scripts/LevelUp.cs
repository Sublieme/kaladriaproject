﻿using System.Collections;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LevelUp : MonoBehaviour {
    private List<Unit> units;
    private int nextUnit = 0;
    private int nextStat = 1;
    private FileManager fileScript;
    private float desiredNumber;
    private float initialNumber;
    private float currentNumber;
    private bool animationPlaying = false;
    private Skill skillSelected;
    private List<Skill> unitSkills;
    GameObject unitData;
    TextMeshProUGUI[] unitTexts;
    Unit updatedUnit;

    void Awake() {
        fileScript = GetComponent<FileManager>();
        Screen.fullScreen = true;
    }
    void Start() {
        Screen.fullScreen = true;
        string[] unitNames = FileManager.instance.UnitsToCreate();
        units = new List<Unit>();
        foreach(string unit in unitNames) {
            units.Add(FileManager.instance.CreateUnit(unit));
        }
        LoadUnit();

    }

    public void SkillSelected(int numberofSkill) {

        if (numberofSkill == 0) {
            GameObject.Find("Skill2").GetComponent<Image>().color = new Color32(255, 255, 255, 140);
            GameObject.Find("Skill1").GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        } else {
            GameObject.Find("Skill1").GetComponent<Image>().color = new Color32(255, 255, 255, 140);
            GameObject.Find("Skill2").GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        
        if (updatedUnit.Level == 3) {
            skillSelected = unitSkills[numberofSkill];
            GameObject.Find("SkillText").GetComponent<TextMeshProUGUI>().text = skillSelected.Info;
        } else if (updatedUnit.Level == 7) {
            skillSelected = unitSkills[numberofSkill+2];
            GameObject.Find("SkillText").GetComponent<TextMeshProUGUI>().text = skillSelected.Info; 
        } else if (updatedUnit.Level == 9) {
            skillSelected = unitSkills[numberofSkill + 4];
            GameObject.Find("SkillText").GetComponent<TextMeshProUGUI>().text = skillSelected.Info;
        }

    }

    public void NextUnit() {
            if (updatedUnit.Level == 3) {
                updatedUnit.Skill2 = skillSelected;
            } else if (updatedUnit.Level == 7) {
                updatedUnit.Skill3 = skillSelected;
            }
            else if (updatedUnit.Level == 9) {
                updatedUnit.Skill4 = skillSelected;
            }
            FileManager.instance.SaveUnit(updatedUnit);

        if(nextUnit < units.Count) { 
            LoadUnit();
        } else {
            FileManager.instance.SaveGame();
            SceneManager.LoadScene("DialogScene");
        }
    }

    public void LoadUnit() {
        if (GameObject.Find("SkillText") != null)
            GameObject.Find("SkillText").GetComponent<TextMeshProUGUI>().text = "";
        Screen.fullScreen = true;
        Camera.main.gameObject.GetComponents<AudioSource>()[0].Play();
        Unit originalUnit = units[nextUnit];
        unitSkills = FileManager.instance.SkillList(originalUnit.Name);
        unitData = GameObject.Find("UnitData");
        unitTexts = unitData.GetComponentsInChildren<TextMeshProUGUI>();
        System.Random rng = new System.Random();
        currentNumber = originalUnit.Hp;
        initialNumber = currentNumber;        

        if (originalUnit.Name.Equals("Alke")) {
            updatedUnit = new Unit(originalUnit.Name, originalUnit.Level + 1, originalUnit.Hp + rng.Next(2, 6), originalUnit.Atk + rng.Next(2, 5), originalUnit.Def + rng.Next(0, 4), originalUnit.Res + rng.Next(0, 2) + rng.Next(0, 2), originalUnit.Spd, originalUnit.Mov,originalUnit.Skill1, originalUnit.Skill2, originalUnit.Skill3, originalUnit.Skill4);
            desiredNumber = updatedUnit.Hp;
        }
        if (originalUnit.Name.Equals("Pleyanie")) {
            updatedUnit = new Unit(originalUnit.Name, originalUnit.Level + 1, originalUnit.Hp + rng.Next(1, 4), originalUnit.Atk + rng.Next(1, 2), originalUnit.Def + rng.Next(0, 1), originalUnit.Res + rng.Next(2, 5), originalUnit.Spd + rng.Next(1, 4), originalUnit.Mov , originalUnit.Skill1, originalUnit.Skill2, originalUnit.Skill3, originalUnit.Skill4);
            desiredNumber = updatedUnit.Hp;
        }
        if (originalUnit.Name.Equals("Calem")) {
            updatedUnit = new Unit(originalUnit.Name, originalUnit.Level + 1, originalUnit.Hp + rng.Next(1, 5), originalUnit.Atk + rng.Next(1, 4), originalUnit.Def + rng.Next(0, 2), originalUnit.Res + rng.Next(0, 2), originalUnit.Spd + rng.Next(1, 6), originalUnit.Mov, originalUnit.Skill1, originalUnit.Skill2, originalUnit.Skill3, originalUnit.Skill4);
            desiredNumber = updatedUnit.Hp;
        }
        if (originalUnit.Name.Equals("Zac")) {
            updatedUnit = new Unit(originalUnit.Name, originalUnit.Level + 1, originalUnit.Hp + rng.Next(1, 2), originalUnit.Atk + rng.Next(1, 4), originalUnit.Def + rng.Next(0, 1), originalUnit.Res + rng.Next(0, 1), originalUnit.Spd + rng.Next(1, 3), originalUnit.Mov, originalUnit.Skill1, originalUnit.Skill2, originalUnit.Skill3, originalUnit.Skill4);
            desiredNumber = updatedUnit.Hp;
        }
        if (originalUnit.Name.Equals("Karia")) {
            updatedUnit = new Unit(originalUnit.Name, originalUnit.Level + 1, originalUnit.Hp + rng.Next(1, 4), originalUnit.Atk + rng.Next(2, 5), originalUnit.Def + rng.Next(1, 2), originalUnit.Res + rng.Next(1, 3), originalUnit.Spd + rng.Next(1, 5), originalUnit.Mov, originalUnit.Skill1, originalUnit.Skill2, originalUnit.Skill3, originalUnit.Skill4);
            desiredNumber = updatedUnit.Hp;
        }

        if (updatedUnit.Level != 3 && updatedUnit.Level != 7 && updatedUnit.Level != 9) {
            GameObject.Find("UnitData").transform.Find("Skill1").gameObject.SetActive(false);
            GameObject.Find("UnitData").transform.Find("Skill2").gameObject.SetActive(false);
            GameObject.Find("UnitData").transform.Find("SkillBox").gameObject.SetActive(false);
            GameObject.Find("UnitData").transform.Find("NewSkill").gameObject.SetActive(false);

        } else {
            GameObject.Find("UnitData").transform.Find("Skill1").gameObject.SetActive(true);
            GameObject.Find("UnitData").transform.Find("Skill2").gameObject.SetActive(true);
            GameObject.Find("UnitData").transform.Find("SkillBox").gameObject.SetActive(true);
            GameObject.Find("UnitData").transform.Find("NewSkill").gameObject.SetActive(true);
        }
        if (updatedUnit.Level == 3) {

            StartCoroutine(ChargeImage(GameObject.Find("Skill1").GetComponent<Image>(), unitSkills[0].TypeOfSkill.ToString()));
            StartCoroutine(ChargeImage(GameObject.Find("Skill2").GetComponent<Image>(), unitSkills[1].TypeOfSkill.ToString()));

        } else if (updatedUnit.Level == 7) {

            StartCoroutine(ChargeImage(GameObject.Find("Skill1").GetComponent<Image>(), unitSkills[3].TypeOfSkill.ToString()));
            StartCoroutine(ChargeImage(GameObject.Find("Skill2").GetComponent<Image>(), unitSkills[4].TypeOfSkill.ToString()));

        } else if (updatedUnit.Level == 9) {
            StartCoroutine(ChargeImage(GameObject.Find("Skill1").GetComponent<Image>(), unitSkills[5].TypeOfSkill.ToString()));
            StartCoroutine(ChargeImage(GameObject.Find("Skill2").GetComponent<Image>(), unitSkills[6].TypeOfSkill.ToString()));
            GameObject.Find("SkillText").GetComponent<TextMeshProUGUI>().text = skillSelected.Info;
        }
        StartCoroutine(ChargeImage(GameObject.Find("Avatar").GetComponent<Image>(), originalUnit.Name));


        GameObject.Find("Name").GetComponent<TextMeshProUGUI>().text = originalUnit.Name; 
        foreach (TextMeshProUGUI text in unitTexts) {
            if (text.name.Equals("Lvl")) {
                text.text = "Nivel " + updatedUnit.Level.ToString();
            }
            if (text.name.Equals("Hp")) {
                text.text = "Hit Points: " + originalUnit.Hp.ToString();
            }
            if (text.name.Equals("Atk")) {
                text.text = "Ataque: " + originalUnit.Atk.ToString();
            }
            if (text.name.Equals("Def")) {
                text.text = "Defensa: " + originalUnit.Def.ToString();
            }
            if (text.name.Equals("Res")) {
                text.text = "Resistencia: " + originalUnit.Res.ToString();
            }
            if (text.name.Equals("Spd")) {
                text.text = "Velocidad: " + originalUnit.Spd.ToString();
            }
        }
        animationPlaying = true;
    }

    public void Update() {
        if (!Screen.fullScreen) {
            Screen.fullScreen = true;
        }
        if (animationPlaying) {
            if (currentNumber != desiredNumber) {
                if (initialNumber < desiredNumber) {
                    currentNumber += (2.0f * Time.deltaTime) * (desiredNumber - initialNumber);
                    if (currentNumber >= desiredNumber) {
                        currentNumber = desiredNumber;
                    }
                }
            }

            if (nextStat < 6) {
                unitTexts[nextStat].text = unitTexts[nextStat].text.Split(':')[0] + ": ";
                unitTexts[nextStat].text = unitTexts[nextStat].text + currentNumber.ToString("0");
                if (currentNumber == desiredNumber) {
                    nextStat += 1;
                    switch (nextStat) {
                        case 1:
                            initialNumber = units[nextUnit].Hp;
                            currentNumber = initialNumber;
                            desiredNumber = updatedUnit.Hp;
                            break;
                        case 2:
                            initialNumber = units[nextUnit].Atk;
                            currentNumber = initialNumber;
                            desiredNumber = updatedUnit.Atk;
                            break;
                        case 3:
                            initialNumber = units[nextUnit].Def;
                            currentNumber = initialNumber;
                            desiredNumber = updatedUnit.Def;
                            break;
                        case 4:
                            initialNumber = units[nextUnit].Res;
                            currentNumber = initialNumber;
                            desiredNumber = updatedUnit.Res;
                            break;
                        case 5:
                            initialNumber = units[nextUnit].Spd;
                            currentNumber = initialNumber;
                            desiredNumber = updatedUnit.Spd;
                            break;



                    }
                } 
            } else {
                animationPlaying = false;
                nextStat = 1;
            }
        }

    }

    public void OnMouseDown() {
        if (updatedUnit.Level != 3 && updatedUnit.Level != 7 && updatedUnit.Level != 9) {
            nextUnit += 1;
            NextUnit();

        } else if(skillSelected != null) {
            nextUnit += 1;
            NextUnit();
            
        }
        
    }

    private IEnumerator ChargeImage(Image image, string name) {

        string path = Application.streamingAssetsPath + "/" + name + ".png";


        byte[] imgData;
        Texture2D tex = new Texture2D(2, 2);

        //Check if we should use UnityWebRequest or File.ReadAllBytes
        if (path.Contains("://") || path.Contains(":///")) {
            UnityWebRequest www = UnityWebRequest.Get(path);
            yield return www.SendWebRequest();
            imgData = www.downloadHandler.data;
        } else {
            imgData = System.IO.File.ReadAllBytes(path);
        }

        //Load raw Data into Texture2D 
        tex.LoadImage(imgData);

        //Convert Texture2D to Sprite
        Vector2 pivot = new Vector2(0.5f, 0.5f);
        Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), pivot, 100.0f);

        image.sprite = sprite;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarAnimation : MonoBehaviour {

    private float _targetHealth;
    private float _actualHealth;
    public float TargetHealth { get => _targetHealth; set => _targetHealth = value; }
    public float ActualHealth { get => _actualHealth; set => _actualHealth = value; }

    public void UpdateHealth() {
        transform.Find("Bar").localScale = new Vector2(_actualHealth, 1f);
        if (_targetHealth > _actualHealth) {
            _actualHealth = _actualHealth + 0.1f;
            if (_actualHealth <= _targetHealth) {
                _actualHealth = _targetHealth;
                CancelInvoke();
            }
        } else {
            _actualHealth = _actualHealth - 0.1f;
            if (_actualHealth <= _targetHealth) {
                CancelInvoke();
            }
        }
    }

}

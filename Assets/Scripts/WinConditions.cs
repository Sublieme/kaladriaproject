﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WinConditions : MonoBehaviour
{
    public void DestroyConditions() {
        Destroy(gameObject);
        Camera.main.gameObject.GetComponents<AudioSource>()[1].Play();
    }


    public void LoadConditions(string victory, string defeat) {
        transform.Find("Victory").Find("VictoryText").gameObject.GetComponent<TextMeshProUGUI>().text = victory;
        transform.Find("Defeat").Find("DefeatText").gameObject.GetComponent<TextMeshProUGUI>().text = defeat;
        Invoke("DestroyConditions", 5f);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitialScreenMenu : MonoBehaviour
{
    private FileManager _fileScript;
    public void NewGame() {

        StartCoroutine(LoadNewGame("DialogScene"));

    }
    public void Awake() {
        Camera.main.gameObject.GetComponents<AudioSource>()[0].volume = PlayerPrefs.GetFloat("MusicValue", 0.75f);
        Camera.main.gameObject.GetComponents<AudioSource>()[1].volume = PlayerPrefs.GetFloat("SoundValue", 0.75f);
        _fileScript = GetComponent<FileManager>();
        Screen.fullScreen = true;
    }

    private IEnumerator LoadNewGame(string scene) {
        if (Directory.Exists(Application.persistentDataPath + "/GameFiles")) {
            Directory.Delete(Application.persistentDataPath + "/GameFiles", true);
        }
        Directory.CreateDirectory(Application.persistentDataPath + "/GameFiles");
        if (Directory.Exists(Application.persistentDataPath + "/GameFiles")) {

            StartCoroutine( FileManager.instance.CopyFiles("Alke"));
            StartCoroutine(FileManager.instance.CopyFiles("AlkeSkills"));
            StartCoroutine(FileManager.instance.CopyFiles("Pleyanie"));
            StartCoroutine(FileManager.instance.CopyFiles("PleyanieSkills"));
            StartCoroutine(FileManager.instance.CopyFiles("Calem"));
            StartCoroutine(FileManager.instance.CopyFiles("CalemSkills"));
            StartCoroutine(FileManager.instance.CopyFiles("Zac"));
            StartCoroutine(FileManager.instance.CopyFiles("ZacSkills"));
            StartCoroutine(FileManager.instance.CopyFiles("Karia"));
            StartCoroutine(FileManager.instance.CopyFiles("KariaSkills"));


            StartCoroutine(FileManager.instance.CopyFiles("Guerrero"));
            StartCoroutine(FileManager.instance.CopyFiles("Bandida"));
            StartCoroutine(FileManager.instance.CopyFiles("Exploradora"));
            StartCoroutine(FileManager.instance.CopyFiles("Sombra"));
            StartCoroutine(FileManager.instance.CopyFiles("Espiritista"));
            StartCoroutine(FileManager.instance.CopyFiles("Arla"));




            StartCoroutine(FileManager.instance.CopyFiles("Configuration"));

            StartCoroutine(FileManager.instance.CopyFiles("Map1"));
            StartCoroutine(FileManager.instance.CopyFiles("Map2"));
            StartCoroutine(FileManager.instance.CopyFiles("Map3"));
            StartCoroutine(FileManager.instance.CopyFiles("Map4"));
            StartCoroutine(FileManager.instance.CopyFiles("Map5"));
            StartCoroutine(FileManager.instance.CopyFiles("Map6"));
            StartCoroutine(FileManager.instance.CopyFiles("Map7"));
            StartCoroutine(FileManager.instance.CopyFiles("Map8"));
            StartCoroutine(FileManager.instance.CopyFiles("Map9"));
            StartCoroutine(FileManager.instance.CopyFiles("Map10"));

            StartCoroutine(FileManager.instance.CopyFiles("Dialog0"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog1"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog2"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog3"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog4"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog5"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog6"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog7"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog8"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog9"));
            StartCoroutine(FileManager.instance.CopyFiles("Dialog10"));

            Camera.main.gameObject.GetComponents<AudioSource>()[1].Play();
            yield return new WaitForSeconds(1.0f);
            SceneManager.LoadScene(scene);
        }

    }

    

    public void ContinueGame() {
        StartCoroutine(LoadContinueGame("DialogScene"));
    }

    private IEnumerator LoadContinueGame(string scene) {
        string[] configuration = FileManager.instance.GetConfigurationFile();
        if (configuration != null) {
            Camera.main.gameObject.GetComponents<AudioSource>()[1].Play();
            yield return new WaitForSeconds(1.0f);
            SceneManager.LoadScene(scene);
        }

    }

}

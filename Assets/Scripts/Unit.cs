﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit
{
    private string _name;
    private int _level;
    private int _hp;
    private int _currentHp;
    private int _atk;
    private int _def;
    private int _res;
    private int _mov;
    private int _spd;
    private Skill _skill1;
    private Skill _skill2;
    private Skill _skill3;
    private Skill _skill4;
    private bool _hasMoved;
    private bool _hasAttacked;



    public int Spd { get => _spd; set => _spd = value; }
    public string Name { get => _name; set => _name = value; }
    public int Hp { get => _hp; set => _hp = value; }
    public int Atk { get => _atk; set => _atk = value; }
    public int Def { get => _def; set => _def = value; }
    public int Res { get => _res; set => _res = value; }
    public int Mov { get => _mov; set => _mov = value; }
    public bool HasMoved { get => _hasMoved; set => _hasMoved = value; }
    public bool HasAttacked { get => _hasAttacked; set => _hasAttacked = value; }
    public int Level { get => _level; set => _level = value; }
    public Skill Skill1 { get => _skill1; set => _skill1 = value; }
    public Skill Skill2 { get => _skill2; set => _skill2 = value; }
    public Skill Skill3 { get => _skill3; set => _skill3 = value; }
    public Skill Skill4 { get => _skill4; set => _skill4 = value; }
    public int CurrentHp { get => _currentHp; set => _currentHp = value; }

    public Unit(string name, int level, int hp, int atk, int def, int res, int spd, int mov, Skill skill1, Skill skill2, Skill skill3, Skill skill4) {
        Name = name;
        Level = level;
        Hp = hp;
        Atk = atk;
        Def = def;
        Res = res;
        Mov = mov;
        Spd = spd;
        Skill1 = skill1;
        Skill2 = skill2;
        Skill3 = skill3;
        Skill4 = skill4;
        HasMoved = false;
        HasAttacked = false;
        CurrentHp = Hp;
    }

    public Unit() {
        Name = "mock";
        Level = -1;
        Hp = 1000;
        Atk = -1;
        Def = -1;
        Res = -1;
        Mov = -1;
        Spd = -1;
        Skill1 = null;
        Skill2 = null;
        Skill3 = null;
        HasMoved = false;
        HasAttacked = false;
        CurrentHp = Hp;
    }

    public override string ToString() {
        return Name+"|"+CurrentHp+"|"+Atk+"|"+Def+"|"+Res+"|"+Spd;
    }
   /* public override bool Equals(System.Object obj) {
        // If parameter is null return false.
        if (obj == null) {
            return false;
        }

        // If parameter cannot be cast to Point return false.
        Unit unit = obj as Unit;
        if ((System.Object)unit == null) {
            return false;
        }

        // Return true if the fields match:
        return unit.Name.Equals(Name);
    }

    /*public bool Equals(Unit unit) {
        // If parameter is null return false:
        if ((object)unit == null) {
            return false;
        }

        // Return true if the fields match:
        return unit.Name.Equals(Name);
    }

    public bool Equals(string unitName) {
        // If parameter is null return false:
        if ((object)unitName == null) {
            return false;
        }

        // Return true if the fields match:
        return unitName.Equals(Name);
    }*/

}

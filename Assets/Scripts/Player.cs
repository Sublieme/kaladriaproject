﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    private List<Unit> army;

    public List<Unit> Army { get => army; set => army = value; }
    public Player() {
        army = new List<Unit>();
    }

}

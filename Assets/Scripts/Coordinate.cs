﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coordinate {
    private int _x;
    private int _y;

    public Coordinate(int x, int y) {
        X = x;
        Y = y;
    }
    public override bool Equals(System.Object obj) {
        // If parameter is null return false.
        if (obj == null) {
            return false;
        }

        // If parameter cannot be cast to Point return false.
        Coordinate p = obj as Coordinate;
        if ((System.Object)p == null) {
            return false;
        }

        // Return true if the fields match:
        return (X == p.X) && (Y == p.Y);
    }

    public bool Equals(Coordinate p) {
        // If parameter is null return false:
        if ((object)p == null) {
            return false;
        }

        // Return true if the fields match:
        return (X == p.X) && (Y == p.Y);
    }

    public override int GetHashCode() {
        return X ^ Y;
    }
    public int X { get => _x; set => _x = value; }
    public int Y { get => _y; set => _y = value; }
}

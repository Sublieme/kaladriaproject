﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageAnimation : MonoBehaviour {

    private Image _image;
    private SpriteRenderer _sprite;
    RectTransform _transform;

    void Start() {
        _image = GetComponent<Image>();
        _sprite = GetComponent<SpriteRenderer>();
        _transform = GetComponent<RectTransform>();

    }

    public void LoadAnimation(string unit) {

        Animator anim = GetComponent<Animator>();
        anim.Play(unit, -1, 0f); ;
    }

    void Update() {
        
        
        _image.sprite = _sprite.sprite;
        if (_image.sprite != null) {
            /*if (_image.sprite.texture.height < 900) {
                _transform.localPosition = new Vector2(transform.localPosition.x, 200f);
            } else {
                _transform.localPosition = new Vector2(transform.localPosition.x, 300f);
            }*/
            _transform.sizeDelta = new Vector2(_image.sprite.texture.width, _image.sprite.texture.height);
        }
    }
}

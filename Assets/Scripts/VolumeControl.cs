﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour
{
    public AudioMixer mixerMusic;
    public Slider musicSlider;
    public Slider soundSlider;

    void Awake() {
        musicSlider.value = PlayerPrefs.GetFloat("MusicValue", 0.75f);
        SetMusicLevel(musicSlider.value);
        soundSlider.value = PlayerPrefs.GetFloat("MusicValue", 0.75f);
        SetSoundLevel(soundSlider.value);
        gameObject.SetActive(false);
        

    }
    public void SetMusicLevel(float sliderValue) {
        mixerMusic.SetFloat("MusicValue", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("MusicValue", sliderValue);
    }

    public void SetSoundLevel(float sliderValue) {
        mixerMusic.SetFloat("SoundValue", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("SoundValue", sliderValue);
    }
}

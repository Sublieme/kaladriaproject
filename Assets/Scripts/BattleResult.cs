﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleResult
{
    private int _oHP;
    private int _tHP;
    private int _oDmg;
    private int _tDmg;
    private int _numberOfAttacks;

    public int OHP { get => _oHP; set => _oHP = value; }
    public int THP { get => _tHP; set => _tHP = value; }
    public int ODmg { get => _oDmg; set => _oDmg = value; }
    public int TDmg { get => _tDmg; set => _tDmg = value; }
    public int NumberOfAttacks { get => _numberOfAttacks; set => _numberOfAttacks = value; }

    public BattleResult(int oHP, int tHP, int oDmg, int tDmg, int nAttacks) {

        _oHP = oHP;
        _tHP = tHP;
        _oDmg = oDmg;
        _tDmg = tDmg;
        _numberOfAttacks = nAttacks;    
        
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class FileManager : MonoBehaviour {

    public static FileManager instance = null;

    void Awake() {
        if (instance == null) {

            instance = this;
        }


        else if (instance != this) {
            Destroy(gameObject);
        }
    }

    public List<Skill> SkillList(string name) {
        List<Skill> skillList = new List<Skill>();
        string[] skillText = null;
        string txtData;
        string filePath = Application.persistentDataPath + "/GameFiles/" + name + "Skills.txt";

        if (filePath.Contains("://") || filePath.Contains(":///")) {
            /* UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;*/
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }
            txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath);
        }
        skillText = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string line in skillText) {
            string actualLine = line.Trim('\r');
            skillList.Add(CreateSkill(actualLine));
        }

        return skillList;
    }


    public Unit CreateUnit(string name) {
        Unit unit = new Unit();
        string txtData;
        string filePath = null;
        filePath = Application.persistentDataPath + "/GameFiles/" + name + ".txt";


        if (filePath.Contains("://") || filePath.Contains(":///")) {
            /* UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;*/
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }
            txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath);
        }

        byte[] txtBytes = System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(txtData);
        txtData = System.Text.Encoding.GetEncoding("iso-8859-1").GetString(txtBytes);
        string[] unitText = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);


        unit.Name = unitText[0].Trim('\r');
        unit.Level = int.Parse(unitText[1].Trim('\r'));
        unit.Hp = int.Parse(unitText[2].Trim('\r'));
        unit.Atk = int.Parse(unitText[3].Trim('\r'));
        unit.Def = int.Parse(unitText[4].Trim('\r'));
        unit.Res = int.Parse(unitText[5].Trim('\r'));
        unit.Spd = int.Parse(unitText[6].Trim('\r'));
        unit.Mov = int.Parse(unitText[7].Trim('\r'));
        unit.Skill1 = CreateSkill(unitText[8].Trim('\r'));
        unit.Skill2 = CreateSkill(unitText[9].Trim('\r'));
        unit.Skill3 = CreateSkill(unitText[10].Trim('\r'));
        unit.Skill4 = CreateSkill(unitText[11].Trim('\r'));
        unit.CurrentHp = unit.Hp;

        return unit;

    }

    private Skill CreateSkill(string line) {
        string[] dividedLine = line.Split('|');
        CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
        ci.NumberFormat.CurrencyDecimalSeparator = ".";
        float fl = float.Parse(dividedLine[3], NumberStyles.Any, ci);
        return new Skill(dividedLine[0], dividedLine[1], int.Parse(dividedLine[2]), fl, (TypeOfSkill)Enum.Parse(typeof(TypeOfSkill), dividedLine[4]));
    }

    public void GetAdditionalInfo(int level, List<string> additionalInfo) {
        string[] mapText = null;
        string txtData;

        string filePath = Application.persistentDataPath + "/GameFiles/Map" + level.ToString() + ".txt";

        if (filePath.Contains("://") || filePath.Contains(":///")) {
            /* UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;*/
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }
            txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath);
        }
        mapText = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        foreach(string line in mapText) {
            additionalInfo.Add(line);
        }
        //additionalInfo.Add(mapText[0].Trim('\r').Split(':')[1]);
        //additionalInfo.Add(mapText[mapText.Length - 2].Trim('\r').Split(':')[1]);
        //additionalInfo.Add(mapText[mapText.Length - 1].Trim('\r').Split(':')[1]);

    }



    public void CreateBoard(int level, Tile[,]board) {
        string[] mapText = null;
        string txtData;
        for (int x = 0; x < board.GetLength(0); x++) {
            for (int y = 0; y < board.GetLength(1); y++) {
                board[x, y] = new Tile();
            }
        }
        string filePath = Application.persistentDataPath + "/GameFiles/Map" + level.ToString() + ".txt";

        if (filePath.Contains("://") || filePath.Contains(":///")) {
            /* UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;*/
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }
            txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath);
        }
        mapText = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        char[] charsToTrim = { '{', '}', '\r', '\n' };

        foreach (String line in mapText) {
            string lineTrimmed = line.Trim(charsToTrim);
            if (!lineTrimmed.Contains("Type") && !lineTrimmed.Contains("Victoria") && !lineTrimmed.Contains("Derrota")) {
                string[] unitPosition = lineTrimmed.Split('|');
                Coordinate unitCoords = new Coordinate((int)char.GetNumericValue(unitPosition[0][0]), (int)char.GetNumericValue(unitPosition[0][2]));
                board[unitCoords.X, unitCoords.Y].CanWalk = false;
                if (!lineTrimmed.Contains("Tree") && !lineTrimmed.Contains("Rock")) {
                    board[unitCoords.X, unitCoords.Y].Unit = CreateUnit(unitPosition[1]);
                }

            }
        }

        

    }
    public void SaveGame() {
        string filePath = Application.persistentDataPath + "/GameFiles/Configuration.txt";
        string[] lines = null;
        string txtData;
        if (filePath.Contains("://") || filePath.Contains(":///")) {
            /* UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;*/
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }
            txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath, System.Text.Encoding.GetEncoding("iso-8859-1"));
        }

        lines = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        int aux = int.Parse(lines[0].Split(':')[1]) + 1;
        lines[0] = lines[0].Split(':')[0]+ ":" + aux.ToString();

        aux = int.Parse(lines[1].Split(':')[1]) + 1;
        lines[1] = lines[1].Split(':')[0] + ":" + aux.ToString();

        if (lines[0].Contains("3")) {
            lines[2] = lines[2].Trim('\r') + ",Calem";
        }
        if (lines[0].Contains("5")) {
            lines[2] = lines[2].Trim('\r') + ",Zac";
        }
        if (lines[0].Contains("7")) {
            lines[2] = lines[2].Trim('\r') + ",Karia";
        }


        string saveFile = lines[0] + "\r\n" + lines[1] + "\r\n" + lines[2] + "\r\n";
        File.WriteAllText(filePath, saveFile, System.Text.Encoding.GetEncoding("iso-8859-1"));
        
    }

    public IEnumerator LoadLevel(System.Action<int> callback) {
        string configPath = Application.persistentDataPath + "/GameFiles/Configuration.txt";
        string txtData;



        if (configPath.Contains("://") || configPath.Contains(":///")) {
            UnityWebRequest www = UnityWebRequest.Get(configPath);
            yield return www.SendWebRequest();
            txtData = www.downloadHandler.text;
        } else {
            txtData = System.IO.File.ReadAllText(configPath);
        }



        string[] lines = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        lines = System.IO.File.ReadAllLines(configPath, System.Text.Encoding.GetEncoding("iso-8859-1"));



        callback(int.Parse(lines[0].Split(':')[1]));
    }

    public void SaveUnit(Unit unit) {

        string filePath = Application.persistentDataPath + "/GameFiles/" + unit.Name + ".txt";
        string unitText = unit.Name + "\r\n" + unit.Level + "\r\n" + unit.Hp.ToString() + "\r\n" + unit.Atk.ToString() + "\r\n" + unit.Def.ToString() + "\r\n" +
                          unit.Res.ToString() + "\r\n" + unit.Spd.ToString() + "\r\n" + unit.Mov.ToString() + "\r\n" +
                          unit.Skill1.Name + "|" + unit.Skill1.Info + "|" + unit.Skill1.Range.ToString() + "|" + unit.Skill1.Modifier.ToString() + "|" + unit.Skill1.TypeOfSkill.ToString() + "\r\n" +
                          unit.Skill2.Name + "|" + unit.Skill2.Info + "|" + unit.Skill2.Range.ToString() + "|" + unit.Skill2.Modifier.ToString() + "|" + unit.Skill2.TypeOfSkill.ToString() + "\r\n" +
                          unit.Skill3.Name + "|" + unit.Skill3.Info + "|" + unit.Skill3.Range.ToString() + "|" + unit.Skill3.Modifier.ToString() + "|" + unit.Skill3.TypeOfSkill.ToString() + "\r\n" +
                          unit.Skill4.Name + "|" + unit.Skill4.Info + "|" + unit.Skill4.Range.ToString() + "|" + unit.Skill4.Modifier.ToString() + "|" + unit.Skill4.TypeOfSkill.ToString() + "\r\n";

        File.WriteAllText(filePath, unitText, System.Text.Encoding.GetEncoding("iso-8859-1"));
    }

    public string[] UnitsToCreate() {
        string[] units = { "", "" };
        List<string> unitNames = new List<string>();
        GetConfigurationFile(unitNames);
        if (unitNames != null) {
            units = unitNames[2].Split(',');
        }
        return units;
    }

    public string[] GetConfigurationFile() {
        string[] config = null;
        List<string> configurationFileList = new List<string>();
        GetConfigurationFile(configurationFileList);
        if(configurationFileList.Count > 0) {
            config = new string[3]{configurationFileList[0],configurationFileList[1], configurationFileList[2]};
        }
        return config;
    }

    public IEnumerator CopyFiles(string fileToCopy) {
        string filePath = Application.streamingAssetsPath + "/Data/" + fileToCopy + ".txt";

        string txtData; 
        if (filePath.Contains("://") || filePath.Contains(":///")) {
             UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;
            //WWW reader = new WWW(path);
            //while (!reader.isDone) { }
            //txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath);
        }

        File.WriteAllText(Application.persistentDataPath + "/GameFiles/" + fileToCopy + ".txt", txtData);
    }

    private void GetConfigurationFile(List<String> unitNames) {
        string[] configuration = null;
        string txtData;

        string filePath = Application.persistentDataPath + "/GameFiles/Configuration.txt";

        if (filePath.Contains("://") || filePath.Contains(":///")) {
            /* UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;*/
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }
            txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath);
        }
        configuration = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        foreach (string line in configuration) {
            unitNames.Add(line);
        }
        
    }

}
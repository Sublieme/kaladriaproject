﻿using System;
public enum TypeOfSkill {PAttack,MAttack,Heal,Buff,Debuff}
public class Skill
{
    private string _name;
    private int _range;
    private string _info;
    private float _modifier;
    private TypeOfSkill _typeOfSkill;



    public Skill(string name, string info, int range, float modifier, TypeOfSkill typeOfSkill) {
        Name = name;
        Info = info;
        Range = range;
        Modifier = modifier;
        TypeOfSkill = typeOfSkill;
    }

    public string Name { get => _name; set => _name = value; }
    public int Range { get => _range; set => _range = value; }
    public float Modifier { get => _modifier; set => _modifier = value; }
    public TypeOfSkill TypeOfSkill { get => _typeOfSkill; set => _typeOfSkill = value; }
    public string Info { get => _info; set => _info = value; }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleAnimation : MonoBehaviour
{
    private bool _checkEnd;
    public void Start() {
    }
    public void OnMouseDown() {
        
    }

    void Update() {
       if( GameObject.Find("TargetUnitAnim").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f && _checkEnd) {
            gameObject.GetComponent<Canvas>().enabled = false;
            gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }

    public IEnumerator SetupBattleAnimation( BattlesOnTurn battles, int i) {
        if (i < battles.ONames.Count) {
            if(battles.NewTHealthbars[i] < 0) {
                battles.NewTHealthbars[i] = 0;
            }
            if (battles.NewOHealthbars[i] < 0) {
                battles.NewOHealthbars[i] = 0;
            }
            _checkEnd = false;
            //foreach (AnimationState anim in oUnitAnim) { }
            HealthBarAnimation oHealthBarScript = GameObject.Find("OriginHealthBar").GetComponent<HealthBarAnimation>();
            oHealthBarScript.gameObject.transform.Find("Bar").gameObject.transform.localScale = new Vector2(battles.CurrentOHealthbars[i], 1f);
            oHealthBarScript.ActualHealth = battles.CurrentOHealthbars[i];
            oHealthBarScript.TargetHealth = battles.NewOHealthbars[i];

            HealthBarAnimation tHealthBarScript = GameObject.Find("TargetHealthBar").GetComponent<HealthBarAnimation>();
            tHealthBarScript.gameObject.transform.Find("Bar").gameObject.transform.localScale = new Vector2(battles.CurrentTHealthbars[i], 1f);
            tHealthBarScript.ActualHealth = battles.CurrentTHealthbars[i];
            tHealthBarScript.TargetHealth = battles.NewTHealthbars[i];

            oHealthBarScript.InvokeRepeating("UpdateHealth", 0, 0.1f);
            tHealthBarScript.InvokeRepeating("UpdateHealth", 0, 0.1f);


            GameObject.Find("OriginUnitAnim").GetComponent<ImageAnimation>().LoadAnimation(battles.ONames[i] + "Attack");
            GameObject.Find("TargetUnitAnim").GetComponent<ImageAnimation>().LoadAnimation(battles.TNames[i] + "Damage");
            GameObject.Find("TargetUnitAnim").GetComponent<Animator>().speed = 0f;
            yield return new WaitForSeconds(0.8f);
            GameObject.Find("TargetUnitAnim").GetComponent<Animator>().speed = 1f;
            yield return new WaitForSeconds(0.3f);
            StartCoroutine(SetupBattleAnimation(battles, i+1));
        } else{
            _checkEnd = true;
        }
    
    }
}

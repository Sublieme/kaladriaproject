﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CombatStyle { Defensive, Aggresive, Mixed};

public class General 
{
    private List<Unit> army;
    private CombatStyle cStyle;

    public List<Unit> Army { get => army; set => army = value; }
    public CombatStyle CStyle { get => cStyle; set => cStyle = value; }

    public General (CombatStyle combatStyle) {
        army = new List<Unit>();
        cStyle = combatStyle;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.Networking;
using System;

public class DialogManager : MonoBehaviour
{
    public GameObject optionMenu;
    public Texture2D[] dialogImages;
    private List<Dialog> _dialogs;
    private Queue<string> _sentences;
    private int _dialogCounter;
    private int _DialogToLoad;
    private GameObject _avatarBoxRight;
    private GameObject _avatarBoxLeft;
    private GameObject _nameBoxRight;
    private GameObject _nameBoxLeft;
    private byte[] imgData;
    // Use this for initialization
    void Start() {
        _avatarBoxRight = GameObject.Find("AvatarBoxRight");
        _avatarBoxLeft = GameObject.Find("AvatarBoxLeft");
        _nameBoxRight = GameObject.Find("NameBoxRight");
        _nameBoxLeft = GameObject.Find("NameBoxLeft");
        string configPath = Application.persistentDataPath + "/GameFiles/Configuration.txt";
        List<string> configuration = new List<string>();


        _sentences = new Queue<string>();
        _dialogs = new List<Dialog>();
        _dialogCounter = 0;
        StartCoroutine(ProcessFile(configPath,configuration));
        _DialogToLoad = int.Parse(configuration[1].Split(':')[1].Trim('\r'));
        StartDialog(_DialogToLoad);
        optionMenu.SetActive(true);

    }

    public void Awake() {
        Screen.fullScreen = true;
    }

    public void StartDialog(int level) {
        Screen.fullScreen = true;
        string filePath = Application.persistentDataPath + "/GameFiles/Dialog"+level.ToString()+".txt";
        int i = -1;
        _DialogToLoad = level;
        string txtData =null;
        string[] lines;

        if (filePath.Contains("://") || filePath.Contains(":///")) {
            /* UnityWebRequest www = UnityWebRequest.Get(filePath);
             yield return www.SendWebRequest();
             txtData = www.downloadHandler.text;*/
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }
            txtData = reader.text;
        } else {
            txtData = System.IO.File.ReadAllText(filePath);
        }

        lines = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        foreach (string line in lines) {
            if (line.Contains(":")) {
                i++;
                _dialogs.Add(new Dialog());
                _dialogs[i].Name = line.Split(':')[0].Trim('\r');
                
            } else if (line.Contains("|")){
                Image background = GameObject.Find("Background").GetComponent<Image>();
                ChargeImage(background, line.Split('|')[1].Trim('\r'));

            } else {
                _dialogs[i].Sentences.Add(line.Trim('\r'));
            }
        }

        DisplayNextSentence();
    }

    public IEnumerator ProcessFile(string configPath,List<string> configuration) {

        string txtData;

        
        //Check if we should use UnityWebRequest or File.ReadAllBytes
        if (configPath.Contains("://") || configPath.Contains(":///")) {
            UnityWebRequest www = UnityWebRequest.Get(configPath);
            yield return www.SendWebRequest();
            txtData = www.downloadHandler.text;
        } else {
            txtData = System.IO.File.ReadAllText(configPath);
        }

        //Load raw Data into Texture2D 

        string[] lines = txtData.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
        
        foreach (string line in lines)
            configuration.Add(line);
    } 
    public void NextLine() {
        Camera.main.gameObject.GetComponents<AudioSource>()[1].Play();
        DisplayNextSentence();
    }

    public void DisplayNextSentence() {
        if (_sentences.Count > 0) {
            TextMeshProUGUI dialogText = GameObject.Find("DialogText").GetComponent<TextMeshProUGUI>();
            string sentence = _sentences.Dequeue();
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence, dialogText));
        } else {
            if (_dialogCounter < _dialogs.Count) {
                EnqueueDialog(_dialogs[_dialogCounter]);
                //GameObject.Find("NameText").GetComponent<Text>().text = _dialogs[_dialogCounter].Name;
                Image avatar;
                TextMeshProUGUI name;
                if (_dialogCounter % 2 == 0) {
                    _avatarBoxLeft.SetActive(true);
                    _nameBoxLeft.SetActive(true);
                    _avatarBoxRight.SetActive(false);
                    _nameBoxRight.SetActive(false);
                    avatar = GameObject.Find("AvatarLeft").GetComponent<Image>();
                    name = GameObject.Find("NameTextLeft").GetComponent<TextMeshProUGUI>();
                } else {
                    _avatarBoxLeft.SetActive(false);
                    _nameBoxLeft.SetActive(false);
                    _avatarBoxRight.SetActive(true);
                    _nameBoxRight.SetActive(true);
                    avatar = GameObject.Find("AvatarRight").GetComponent<Image>();
                    name = GameObject.Find("NameTextRight").GetComponent<TextMeshProUGUI>();
                }
                ChargeImage(avatar, _dialogs[_dialogCounter].Name); 
                
                name.text = _dialogs[_dialogCounter].Name;
                _dialogCounter++;
                DisplayNextSentence();
            } else {
                SceneManager.LoadScene("BattleScene");
            }
        }
    }

        IEnumerator TypeSentence(string sentence, TextMeshProUGUI dialogText) {
        GameObject.Find("DialogText");
        dialogText.text = "";
        foreach (char letter in sentence.ToCharArray()) {
            dialogText.text += letter;
            yield return new WaitForSeconds(0.05f);
        }
    }

    private void EnqueueDialog(Dialog actualDialog) {

        foreach (string sentence in actualDialog.Sentences) {
            _sentences.Enqueue(sentence);
        }
    }

    private void ChargeImage(Image image, string name) {
        /*string path = path = Application.streamingAssetsPath + "/" + name + ".png";
        byte[] imgData;
        Texture2D tex = new Texture2D(2, 2);

        //Check if we should use UnityWebRequest or File.ReadAllBytes
        if (path.Contains("://") || path.Contains(":///")) {
            UnityWebRequest www = UnityWebRequest.Get(path);
            yield return www.SendWebRequest();
            imgData = www.downloadHandler.data;
        } else {
            imgData = System.IO.File.ReadAllBytes(path);
        }

        //Load raw Data into Texture2D 
        tex.LoadImage(imgData);

        //Convert Texture2D to Sprite
        */
        foreach (Texture2D tex in dialogImages) {
            if (tex.name.Equals(name)) {
                Vector2 pivot = new Vector2(0.5f, 0.5f);
                Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), pivot, 100.0f);

                image.sprite = sprite;
            }
        }
       

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetRoute
{
    private List<Coordinate> _route;
    private Unit _target;

    public List<Coordinate> Route { get => _route; set => _route = value; }
    public Unit Target { get => _target; set => _target = value; }

   public TargetRoute() {
        _route = new List<Coordinate>();
        _target = new Unit();
    }
    
}

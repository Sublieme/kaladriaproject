﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitBehavior : MonoBehaviour
{

    private Coordinate originCoords;
    private Coordinate actualCoords;
    public Vector3 screenPoint;
    public Vector3 offset;
    public GameObject[] bWheel;


    void OnMouseDown() {
        if (BoardManager.instance.IsMyTurn()) {
            originCoords = new Coordinate((int)this.transform.position.x, (int)this.transform.position.y);
            if (!BoardManager.instance.IsUnitMoving() && !BoardManager.instance.AttackIsActive()) {
                BoardManager.instance.ShowUnitStats(originCoords);
            } else if (BoardManager.instance.SwitchUnit(originCoords) && !BoardManager.instance.AttackIsActive()) {
                if (BoardManager.instance.CanMove()) {
                    screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
                    offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(
                        new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
                }
            } else if (!BoardManager.instance.AttackIsActive()) {
                originCoords = new Coordinate((int)this.transform.position.x, (int)this.transform.position.y);
                BoardManager.instance.SwapUnit(originCoords);
            } else {
                BoardManager.instance.MarkAsTarget(originCoords);
            }
        }
            

    }



    void OnMouseDrag() {
        if (BoardManager.instance.CanMove()) {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            transform.position = curPosition;
        }

        

    }

 

    void OnMouseUp() {
        if (BoardManager.instance.CanMove()) {
            actualCoords = new Coordinate((int)System.Math.Round(this.transform.position.x), (int)System.Math.Round(this.transform.position.y));
            BoardManager.instance.Paint(new Coordinate(-1,-1), 2, -1);
            if (BoardManager.instance.MovementValid(originCoords, actualCoords)) {
                this.transform.position = new Vector3(actualCoords.X, actualCoords.Y, 10);
            } else {
                this.transform.position = new Vector3(originCoords.X, originCoords.Y, 10);
                BoardManager.instance.Paint(originCoords, 1, -1);
            }
            
              
        }


    }

}

﻿
public class Tile
{
    private Unit unit;
    private bool canWalk;
    
    public Tile() {
        this.canWalk = true;
    }

    public bool CanWalk { get => canWalk; set => canWalk = value; }
    public Unit Unit { get => unit; set => unit = value; }
}

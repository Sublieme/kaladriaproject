﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlesOnTurn
{
    private List<string> _oNames;
    private List<string> _tNames;
    private List<float> _currentOHealthbars;
    private List<float> _newOHealthbars;
    private List<float> _currentTHealthbars;
    private List<float> _newTHealthbars;
    public BattlesOnTurn() {
        ONames = new List<string>();
        TNames = new List<string>();
        CurrentOHealthbars = new List<float>();
        CurrentTHealthbars = new List<float>();
        NewOHealthbars = new List<float>();
        NewTHealthbars = new List<float>();
        
    }

    public List<string> ONames { get => _oNames; set => _oNames = value; }
    public List<string> TNames { get => _tNames; set => _tNames = value; }
    public List<float> CurrentOHealthbars { get => _currentOHealthbars; set => _currentOHealthbars = value; }
    public List<float> NewOHealthbars { get => _newOHealthbars; set => _newOHealthbars = value; }
    public List<float> CurrentTHealthbars { get => _currentTHealthbars; set => _currentTHealthbars = value; }
    public List<float> NewTHealthbars { get => _newTHealthbars; set => _newTHealthbars = value; }
}

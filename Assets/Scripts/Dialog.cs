﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialog
{
    private string _name;
    private List<string> _sentences;

  public Dialog () {
        _name = null;
        _sentences = new List<string>();

    }

    public string Name { get => _name; set => _name = value; }
    public List<string> Sentences { get => _sentences; set => _sentences = value; }
}

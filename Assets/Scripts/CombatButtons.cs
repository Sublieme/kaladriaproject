﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatButtons : MonoBehaviour
{
    public void BConfirm() {
        BoardManager.instance.ExecuteCombat();
        BoardManager.instance.HideUnitMenu();
    }

    public void bBack() {
        BoardManager.instance.CancelCombat();
    }
}

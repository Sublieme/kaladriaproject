﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    private bool _isMoving = false;
    private List<Coordinate> _pathToMove = null;
    private bool _isToBeDeleted = false;

    public bool IsMoving { get => _isMoving; set => _isMoving = value; }
    public List<Coordinate> PathToMove { get => _pathToMove; set => _pathToMove = value; }
    public bool IsToBeDeleted { get => _isToBeDeleted; set => _isToBeDeleted = value; }

    void OnMouseDown() {
        if (BoardManager.instance.AttackIsActive()){
            BoardManager.instance.MarkAsTarget(new Coordinate((int)this.transform.position.x, (int)this.transform.position.y));
        }else if (!BoardManager.instance.IsUnitMoving()) {

            BoardManager.instance.ShowUnitStats(new Coordinate((int)this.transform.position.x, (int)this.transform.position.y));
        }


    }

    void Update() {
        float step = 4f * Time.deltaTime; // calculate distance to move
        Vector3 actualPosition = this.transform.position;
        Vector3 targetPosition;
        if (_isMoving) {
            if(_pathToMove != null && _pathToMove.Count > 0) {
                targetPosition = new Vector3(_pathToMove[0].X, _pathToMove[0].Y, 10);
                transform.position = Vector3.MoveTowards(transform.position,new Vector3( _pathToMove[0].X, _pathToMove[0].Y,10), step);

                // Check if the position of the cube and sphere are approximately equal.
                if (Vector3.Distance(transform.position, targetPosition) < 0.001f) {
                    transform.position = targetPosition;
                    _pathToMove.RemoveAt(0);
                }
            } else {         
                _isMoving = false;
                BoardManager.instance.CheckMovingUnits();
                if (_isToBeDeleted) {
                    Destroy(gameObject);

                }
            }
        }
    }
}
